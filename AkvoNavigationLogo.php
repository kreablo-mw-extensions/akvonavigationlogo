<?php
/**
 * Copyright (C) 2013 Andreas Jonsson <andreas.jonsson@kreablo.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Extensions
 */

if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

$extDir = dirname( __FILE__ );

$wgExtensionCredits['akvonavigationlogo'][] = array(
	'path' => __FILE__,
	'name' => 'AkvoNavigationLogo',
	'author' => array( 'Andreas Jonsson' ),
	'url' => '',
	'version' => '1.0',
	'descriptionmsg' => 'akvonavigationlogo-desc',
);

$wgExtensionMessagesFiles['AkvoNavigationLogo'] = "$extDir/AkvoNavigationLogo.i18n.php";

$wgHooks['SkinBuildSidebar'][] = 'efAkvoNavigationLogoBuildSidebar';
$wgHooks['SkinTemplateOutputPageBeforeExec'][] = 'efAkvoNavigationSkinTemplateOutputPageBeforeExec';

$weAkvoNavigationLogoPath = false;

function efAkvoNavigationLogoBuildSidebar(Skin $skin, &$bar)
{
    global $weAkvoNavigationLogoPath, $wgExtensionAssetsPath;

    if ($weAkvoNavigationLogoPath === false) {
        $weAkvoNavigationLogoPath = "$wgExtensionAssetsPath/" . basename( dirname( __FILE__ ) ) . "/akvologo.png";
    }

    $hostedBy = $skin->msg( 'akvonavigationlogo-hostedby' )->escaped();
    $url = $dkin->msg( 'akvonavigationlogo-url' )->escaped();
    $bar['akvonavigationlogo-name'] =
            "<span style=\"font-size:smaller\">$hostedBy <a href=\"$url\"><img src=\"$weAkvoNavigationLogoPath\" alt=\"akvo.org\" /></a></span>";

    return true;
}

function efAkvoNavigationSkinTemplateOutputPageBeforeExec(&$skin, &$template) {
	$template->set('remove_my_personal_data',
		'<a href="https://akvo.org/remove-my-personal-data/">' .
		$skin->msg('akvonavigation-remove-my-personal-data')->escaped() .
		'</a>');
}