<?php

class AkvoNavigationLogoHooks {

	public static function buildSidebar(Skin $skin, &$bar)
	{
		global $wgAkvoNavigationLogoPath, $wgExtensionAssetsPath;

		if ($wgAkvoNavigationLogoPath === false) {
			$wgAkvoNavigationLogoPath = "$wgExtensionAssetsPath/" . basename( dirname( __FILE__ ) ) . "/akvologo.png";
		}

		$hostedBy = $skin->msg( 'akvonavigationlogo-hostedby' )->escaped();
		$url = $skin->msg( 'akvonavigationlogo-url' )->escaped();
		$bar['akvonavigationlogo-name'] =
            "<span style=\"font-size:smaller\">$hostedBy <a href=\"$url\"><img src=\"$wgAkvoNavigationLogoPath\" alt=\"akvo.org\" /></a></span>";

		return true;
	}


	public static function onSkinTemplateOutputPageBeforeExec(&$skin, &$template) {
		$template->set('remove_my_personal_data',
			'<a href="https://akvo.org/remove-my-personal-data/">' .
			$skin->msg('akvonavigation-remove-my-personal-data')->escaped() .
			'</a>');
		$template->set('terms_of_service',
			'<a href="https://akvo.org/help/akvo-policies-and-terms-2/akvo-saas-terms-of-service/">' .
			$skin->msg('akvonavigation-terms-of-service')->escaped() .
			'</a>');
		$template->data['footerlinks']['places'][] = 'remove_my_personal_data';
		$template->data['footerlinks']['places'][] = 'terms_of_service';
	}

}