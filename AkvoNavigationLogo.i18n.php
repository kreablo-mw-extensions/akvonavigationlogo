<?php
/**
 * Internationalisation file for extension FetchAndPublish
 *
 * @file
 * @ingroup Extensions
 */

$messages = array();

$messages['en'] = array(
    'akvonavigationlogo-desc' => "Display the Akvo Foundation logo in the sidebar.",
    'akvonavigationlogo-hostedby' => "Hosted and managed by",
    'akvonavigationlogo-url' => 'http://akvo.org/',
    'akvonavigationlogo-name' => '',
);